<?php
class Shop_Region{
    public $region_id;
    public $region_name;
    public $state_id;
    // get regions
    public static function  getRegions(){
        $table="region";
        $output=array();
        if($regions=Shoppers::$DB->select($table,null,null)){
            foreach($regions as $rg){
                $output[]=Shop_Region::fromArray($rg);
            }
           	echo json_encode(array("response"=>"ok","regions"=>$output));
        }
    }
    public static function fromArray($reg){
        $result=new Shop_Region();
        $result->region_name=$reg['region_name'];
        $result->region_id=$reg['region_id'];
        $result->state_id=$reg['state_id'];
        return $result;

    }
    public static function getRegion($region_id){
     $table="region";
     $where="region_id=".$region_id;
    if ($result=Shoppers::$DB->select($table,null,$where)){
     if(count($result)>0){
echo json_encode(array("response"=>"ok","regions"=>$result));
     }
     else{
         echo "failed to get  region";
     }
      
    }
    else{
        echo "region does not exist";
    }

    }
   

}
