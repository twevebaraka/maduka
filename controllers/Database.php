<?php
class Database{
	public static $db="shoppers_hype";


	public static $username="root";
	public static $password="";
	public static $hostname="localhost";

	public static $DB;

	public $link;


	public function connect(){
		$this->link = new mysqli(self::$hostname, self::$username, self::$password,self::$db);
		if ($this->link->connect_errno){
			echo $this->link->connect_error;
		}
	
	}

	
	public function disconnect(){
		$this->link->close();
	}

	public function insert($table,$values){
		if (!self::tableExists($table)) throw new \Exception("Error: table $table does not exist", 1);
		if(!is_array($values) || count($values)<1) throw new \Exception("Error: table invalid parameter supplied", 1);
		$cols="";
		$vals="";
		foreach ($values as $key => $value) {
			$cols.=", ".$key;
			$vals.=", '".mysqli_real_escape_string($this->link,$value)."'";
			
		}
		$cols=substr($cols, 1);
		$vals=substr($vals, 1);
		$query="INSERT INTO $table($cols) VALUES($vals)"; //echo $query;
		if (!$this->link->query($query)) die("Could not insert values ".self::error($query));
		return  $this->link->insert_id;	
		
	}

	public function update($table, $values, $where){
		if (!self::tableExists($table)) throw new \Exception("Error: table $table does not exist", 1);
		if(!is_array($values) || count($values)<1) throw new \Exception("Error: table invalid parameter supplied", 1);
		$queryValues="";
		foreach ($values as $key => $value){
			$cols=$key;
			$vals=is_numeric($value)?$value:"'".mysqli_real_escape_string($this->link,$value)."'";
			$queryValues.=",".$cols."=".$vals;
		}
		if (strlen($queryValues)>0){
			$queryValues=substr($queryValues,1);
			$query="UPDATE $table SET $queryValues WHERE $where"; //echo $query;
			if (!$this->link->query($query)){
				// echo $query;
				//echo "failed to update: ".self::error($query);
				return false;
			}
			return true;
		}else{
			//echo "no values to update or invalid field names";
			// echo $query;
			return false;
		}
	

	}

	public function delete($table, $where){
		if (!self::tableExists($table)) throw new \Exception("Error: table $table does not exist", 1);
		$query="DELETE FROM $table WHERE $where";
		if (!$this->link->query($query)) die("Could not delete row(s)");
		return mysqli_affected_rows($this->link);
	}

	/**
	* return an array of associative array of rows
	*/
	public function select($table,$columns,$where,$count=10000,$start=0,$orderstmt=null){
		if (!self::tableExists($table)) throw new \Exception("Error: table $table does not exist", 1);
		if($columns!=null){
			$cols=""; 
			foreach ($columns as $value) {
				$cols.=",".$value;
			}
			$cols=substr($cols, 1);
		}else{
			$cols="*";
		}
		$where=$where==null?"1=1":$where;
		$orderstmt=$orderstmt==null?"":"ORDER BY ".$orderstmt;
		$query="SELECT $cols FROM $table WHERE $where $orderstmt LIMIT $count OFFSET $start";
		if(!($result=$this->link->query($query))){
			return false;
		}
		$output=array();
		while ($r=$result->fetch_assoc()) {
			$output[]=$r;
		}
		$result->free();
		return $output;
	}


	public function multiple_table_select($tables,$columns,$where,$count=10000,$start=0,$orderstmt=null){
		if($columns!=null){
			$cols=""; 
			foreach ($columns as $value) {
				$cols.=",".$value;
			}
			$cols=substr($cols, 1);
		}else{
			$cols="*";
		}
		$where=$where==null?"1=1":$where;
		$orderstmt=$orderstmt==null?"":"ORDER BY ".$orderstmt;
		$query="SELECT $cols FROM $table WHERE $where $orderstmt LIMIT $count OFFSET $start";
		if(!($result=$this->link->query($query))) return false; 
		$output=array();
		while ($r=$result->fetch_assoc()) {
			$output[]=$r;
		}
		$result->free();
		return $output;
	}

	public function simple_inner_join_select($tables,$columns,$condition,$where,$count=10000,$start=0,$orderstmt=null){
		if($columns!=null){
			$cols=""; 
			foreach ($columns as $value) {
				$cols.=",".$value;
			}
			$cols=substr($cols, 1);
		}else{
			$cols="*";
		}
		$where=$where==null?"1=1":$where;
		$orderstmt=$orderstmt==null?"":"ORDER BY ".$orderstmt;
		$query="SELECT $cols FROM $tables[0] INNER JOIN $tables[1] ON $condition WHERE $where $orderstmt LIMIT $count OFFSET $start";
		// echo $query;
		if(!($result=$this->link->query($query))) return false; 
		$output=array();
		while ($r=$result->fetch_assoc()) {
			$output[]=$r;
		}
		$result->free();
		return $output;
	}

	public function tableExists($table_in){
		$result=$this->link->query("show tables");
		while ($table=$result->fetch_row()) {
			if($table[0]==$table_in){
				return true;
			}
		}
		return false;
	}

	public function start(){
		$this->link->autocommit(false);
	}

	public function rollback(){
		$this->link->rollback();
		$this->link->autocommit(true);
	}

	public function commit(){
		$this->link->commit();
		$this->link->autocommit(true);
	}

	public function error($query){
		return "<br> $query <br>".$this->link->error;
	}
}

class Shoppers{
	public static $DB;
}
 Shoppers::$DB=new Database();
 Shoppers::$DB->connect();
