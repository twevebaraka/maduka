<?php
class Request{
	public $type;

	public static function shouldBe($type, $parameters=array()){
		$RP=null;
		if($type=="POST"){
			if($_SERVER['REQUEST_METHOD']!="POST"){
				//issue an error: it is not a post request
				echo  "The request is not POST request, POST request is required";
				exit;
			}
			$RP=$_POST;
		}else{
			$RP=$_GET;
		}
		foreach ($parameters as $key) {
			if(!isset($RP[$key])){
				//issue an error: parameters missing
				echo "parameter $key not provided on the request";
				exit;
			}
		}
		$request=new Request();
		$request->settype($type);
		return $request;
	}


	public function getParam($key,$default=null,$force_type=null){
		$cr=$_SERVER['REQUEST_METHOD'];
		$value=$default;
		if ($force_type!=null && $cr!=$force_type) {
			$cr=$force_type;
		}
		if ($cr=="POST"){
			$value=isset($_POST[$key])?$_POST[$key]:$default;
		}else{
			$value=isset($_GET[$key])?$_GET[$key]:$default;
		}
		return $value;

	}

	private function setType($type){
		$this->type=$type;
	}

}?>