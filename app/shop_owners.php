<?php
 require_once('../controllers/Shop_owners.php');
 require_once('../controllers/Database.php');
 require_once('../controllers/Request.php');
 require_once('../controllers/Shop_Region.php');
 require_once('../controllers/Shop_District.php');
 $request=Request::shouldBe("POST",array("action"));
 $req_type=$request->getParam("action");
// to register owner or shop
 $host_folder="http://foodly.pe.hu/shopperz/shoppers_hype/";
 $images_folder="images/owners_logo/";
 $gender_folder="images/gender_type_icon/";
  $category_folder="images/category_type_icon/";
  $user_folder="images/user_photo/";
date_default_timezone_set('Africa/Nairobi');
$date_added=date("Y-m-d h:i:s");
if($req_type=="register_owner"){

$req=Request::shouldBe("POST",array("owner_full_name","owner_shop_contact",
"owner_email","shop_name","shop_logo","district_id","shop_street_name","owner_shop_contact","shop_description"));
$owner_fname=$req->getParam("owner_full_name");
$owner_shop_contact=$req->getParam("owner_shop_contact");
$owner_email=$req->getParam("owner_email");
$owner_password=$req->getParam("owner_password");
$shop_name=$req->getParam("shop_name");
$shop_district_id=$req->getParam("district_id");
$shop_street_name=$req->getParam("shop_street_name");
$shop_logo=$req->getParam("shop_logo");
$shop_description=$req->getParam("shop_description");
$logo_name=preg_replace('/\s+/','',$shop_name.time().".png");
$image_path=$images_folder.$logo_name;
$full_logo_path=$host_folder.$image_path;

// generating random keys
 $owner_verification_key=Shop_owners::randomKeys();
// restricting multiple registration
  $where="owner_shop_contact=".$owner_shop_contact;
     $table="shop_owner";
     if($shop_owners=Shoppers::$DB->select($table,null,$where)){
        echo "user with this $owner_shop_contact exists";
     }

     else{
 $shop_details=array("shop_name"=>$shop_name,"district_id"=>$shop_district_id,"street_name"=>$shop_street_name,
"date_added"=>$date_added,"shop_description"=>$shop_description,"shop_logo"=>$full_logo_path);

    $image = base64_decode($shop_logo);
    $fp = fopen("../".$image_path, 'w');
    fwrite($fp, $image);
    if(fclose($fp)){
     echo "Image uploaded"."<br></br>";
    }else{
     echo "Error uploading image";
    }

if(Shop_owners::createOwner($owner_fname,$owner_shop_contact,
$owner_email,$owner_password,$owner_verification_key,$shop_details)){
    // echo $owner_verification_key;
    echo json_encode(array("response"=>"ok","message"=>"shop account registered succesfully"));
}

else{
        echo json_encode(array("response"=>"error","message"=>"shop account registered failed"));
}   
     }
}
 
 // to delete shop and its owner just provide shop_id
 else if($req_type=="delete_shop"){
        $req=Request::shouldBe("POST",array("shop_id"));
       $shop_id=$req->getParam("shop_id");
        Shop_owners::deleteShop($shop_id);
    }
// get shops or owner activated
   else if($req_type=="get_shops"){
    // send one of these to get specific shop or dont send anything to get all shop. jus send the action 
    $req=Request::shouldBe("POST",array("shop_id","owner_id"));
    $shop_id=$req->getParam("shop_id")?$req->getParam("shop_id"):null;
    $owner_id=$req->getParam("owner_id")?$req->getParam("owner_id"):null;
    $status_id=$req->getParam("status_id")?$req->getParam("status_id"):0;
    // to get one shop supply  shop_id
    if($shop_id!=null){
    $where="shop_id=".$shop_id;
    $table="shop";

    $sql="
      select shop.shop_id,shop_name,owner_password,owner_email,rates,shop_description,shop_logo,district.district_id,district_name,region_name,region.region_id,street_name
      ,owner_full_name,owner_shop_contact,activation_key,date_added,view from $table inner join district on
      shop.district_id=district.district_id inner join region on region.region_id=district.region_id inner join shop_owner on shop_owner.owner_id=shop.owner_id inner join shop_view on shop_view.shop_id=shop.shop_id  where shop.shop_id='{$shop_id}' and status=1";
    if($result=Shoppers::$DB->link->query($sql)){
     $output=array();
        while($res=$result->fetch_assoc()){
                    
             $res['shop_name']=$res['shop_name'];
                 $res['shop_id']=$res['shop_id'];
             $res['shop_description']  =$res['shop_description'];
             $res['shop_logo'] =$res['shop_logo'];
             $res['region_name'] =$res['region_name'];
             $res['region_id'] =$res['region_id'];
             $res['district_name'] =$res['district_name'];
             $res['district_id'] =$res['district_id'];
             $res['street_name'] =$res['street_name'];
             $res['date_added'] =$res['date_added'];
             $res['shop_rate'] =$res['rates'];
			 $res['shop_view'] =$res['view'];

             $output=$res;
        }
        echo json_encode(array("response"=>"ok","shop"=>$output));
    }
    else{
        echo json_encode(array("response"=>"error","message"=>"failed to fetch shops"));
    }
    }
 // to get shop_owner
    if($owner_id !=null){
     $where="shop_owner.owner_id=".$owner_id; 

$sql="
select shop.shop_id,shop_name,owner_password,rates,shop_description,shop_logo,district.district_id,district_name,region_name,region.region_id,street_name
      ,owner_full_name,owner_shop_contact,activation_key,owner_email,view from shop inner join district on
      shop.district_id=district.district_id inner join region on region.region_id=district.region_id inner join shop_owner on shop_owner.owner_id=shop.owner_id 
	  inner join shop_view on shop_view.shop_id=shop.shop_id  where status=1 and shop_owner.owner_id='{$owner_id}'
";
if($result=Shoppers::$DB->link->query($sql)){
        while($res=$result->fetch_assoc()){
             $res['shop_name']=$res['shop_name'];
              $res['shop_id']=$res['shop_id'];
             $res['shop_description']  =$res['shop_description'];
             $res['shop_logo'] =$res['shop_logo'];
             $res['region_name'] =$res['region_name'];
             $res['region_id'] =$res['region_id'];
             $res['district_name'] =$res['district_name'];
             $res['district_id'] =$res['district_id'];
             $res['street_name'] =$res['street_name'];
            $res['shop_rate'] =$res['rates'];
            $res['owner_password'] =$res['owner_password'];
            $res['owner_email'] =$res['owner_email'];
            $res['owner_shop_contact'] =$res['owner_shop_contact'];
            $res['owner_full_name'] =$res['owner_full_name'];
            $res['activation_key'] =$res['activation_key'];
			$res['shop_view'] =$res['view'];

             $outputs[]=$res;
        }
        echo json_encode(array("response"=>"ok","shop"=>$outputs));
        
    }
    else{
        echo json_encode(array("response"=>"error","message"=>"failed to fetch shops"));
    }
    }

    // to get all shops activated
    if($owner_id==null && $shop_id==null){
        $table="shop";
       $sql="
       select shop.shop_id,shop_name,owner_password,owner_email,rates,shop_description,shop_logo,district.district_id,district_name,region_name,region.region_id,street_name
      ,owner_full_name,owner_shop_contact,activation_key, view from shop inner join district on
      shop.district_id=district.district_id inner join region on region.region_id=district.region_id inner join shop_owner on shop_owner.owner_id=shop.owner_id inner join shop_view on shop.shop_id=shop_view.shop_id where status=1 ORDER BY shop.rates DESC";
     if($result=Shoppers::$DB->link->query($sql)){
        while($res=$result->fetch_assoc()){
         $res['shop_name']=$res['shop_name'];
              $res['shop_id']=$res['shop_id'];
             $res['shop_description']  =$res['shop_description'];
             $res['shop_logo'] =$res['shop_logo'];
             $res['region_name'] =$res['region_name'];
             $res['region_id'] =$res['region_id'];
             $res['district_name'] =$res['district_name'];
             $res['district_id'] =$res['district_id'];
             $res['street_name'] =$res['street_name'];
            $res['shop_rate'] =$res['rates'];
            $res['owner_password'] =$res['owner_password'];
            $res['owner_email'] =$res['owner_email'];
            $res['owner_shop_contact'] =$res['owner_shop_contact'];
            $res['owner_full_name'] =$res['owner_full_name'];
            $res['activation_key'] =$res['activation_key'];
			$res['shop_view'] =$res['view'];
             $outputs[]=$res;
        }
        echo json_encode(array("response"=>"ok","shop"=>$outputs));
        
    }
    else{
        echo json_encode(array("response"=>"error","message"=>"failed to fetch shops"));
    }
    }
}
// to get all shop activated o not activate # admin supply status
         else if($req_type=="get_shops_admin"){
    $req=Request::ShouldBe("POST",array("status"));
        $status=$req->getParam("status")?$req->getParam("status"):null;
        $sql="
      select shop.shop_id,shop_name,shop_description,shop_logo,district.district_id,district_name,region_name,region.region_id,street_name
      ,owner_full_name,owner_shop_contact,activation_key,status,view, from shop inner join district on
      shop.district_id=district.district_id inner join region on region.region_id=district.region_id inner join shop_owner on shop_owner.owner_id=shop.owner_id inner join shop_view on shop_view.shop_id=shop.shop_id where status='{$status}'"
    ;
        if($result=Shoppers::$DB->link->query($sql)){

        while($res=$result->fetch_assoc()){
                    
             $res['shop_name']=$res['shop_name'];
                 $res['shop_id']=$res['shop_id'];
             $res['shop_description']  =$res['shop_description'];
             $res['shop_logo'] =$res['shop_logo'];
             $res['region_name'] =$res['region_name'];
             $res['region_id'] =$res['region_id'];
             $res['district_name'] =$res['district_name'];
             $res['district_id'] =$res['district_id'];
             $res['street_name'] =$res['street_name'];
			 $res['shop_view'] =$res['view'];
			 $output[]=$res;        }
        echo json_encode(array("res
        ponse"=>"ok","shop"=>$output));
    }
    else{
        echo json_encode(array("response"=>"error","message"=>"failed to fetch shops"));
    }
    
        
         }
    
// to update shop details
else if($req_type=="update_shop"){
$req=Request::shouldBe("POST",array("shop_id","shop_name","shop_description","shop_street_name"));
$shop_id=$req->getParam("shop_id");
$shop_name=$req->getParam("shop_name");
$shop_logo=$req->getParam("shop_logo") !=null?$req->getParam("shop_logo"):null;
$shop_description=$req->getParam("shop_description");
$shop_district_id=$req->getParam("district_id");
$shop_street_name=$req->getParam("shop_street_name");
 $owner_full_name=$req->getParam("owner_full_name");
$owner_email=$req->getParam("owner_email");
$owner_password=$req->getParam("owner_password");
$owner_shop_contact=$req->getParam("owner_shop_contact");


$table_shop="shop";
$table_shop_owner="shop_owner";
$where="shop.shop_id='{$shop_id}'";
//$shop_name=$req->getParam("shop_name")!=null?$req->getParam("shop_name"):null;
if($shop_logo!=null){
    $imageName=preg_replace('/\s+/','',$shop_name.time().".png");
	$image_path=$images_folder.$imageName;
     $full_image_path=$host_folder.$image_path;
   $image = base64_decode($shop_logo);
 

	$fp = fopen("../".$image_path, 'w');
	fwrite($fp, $image);
	if(fclose($fp)){
	 echo "Image uploaded"."<br></br>";
	}else{
	 echo "Error uploading image";
	}
	 $sql="UPDATE $table_shop inner join $table_shop_owner on $table_shop.shop_id=$table_shop_owner.owner_id set $table_shop.shop_name='{$shop_name}',$table_shop.shop_description='{$shop_description}',
	 $table_shop.district_id='{$shop_district_id}',$table_shop.street_name='{$shop_street_name}',$table_shop.shop_logo='{$full_image_path}',$table_shop_owner.owner_full_name='{$owner_full_name}',
	 $table_shop_owner.owner_email='{$owner_email}',$table_shop_owner.owner_shop_contact='{$owner_shop_contact}',$table_shop_owner.owner_password='{$owner_password}' where $where
	";
	if(Shoppers::$DB->link->query($sql)){
	echo json_encode(array("response"=>"ok","message"=>"succesffuly updated"));
}
else{
   echo json_encode(array("response"=>"failed","message"=>"failed to update shop"));
}
	}
	else{

	 
	 $sql="UPDATE $table_shop inner join $table_shop_owner on $table_shop.shop_id=$table_shop_owner.owner_id set $table_shop.shop_name='{$shop_name}',$table_shop.shop_description='{$shop_description}',
	 $table_shop.district_id='{$shop_district_id}',$table_shop.street_name='{$shop_street_name}',$table_shop_owner.owner_full_name='{$owner_full_name}',
	 $table_shop_owner.owner_email='{$owner_email}',$table_shop_owner.owner_shop_contact='{$owner_shop_contact}',$table_shop_owner.owner_password='{$owner_password}' where $where
	";
	if(Shoppers::$DB->link->query($sql)){
	echo json_encode(array("response"=>"ok","message"=>"succesffuly updated"));
}
else{
   echo json_encode(array("response"=>"failed","message"=>"failed to update shop"));
}


	}

}
// update owner details
else if($req_type=="update_shop_owner"){
$req=Request::shouldBe("POST",array("owner_id","owner_full_name","owner_shop_contact","owner_email","owner_password"));
$owner_id=$req->getParam("owner_id");
$owner_full_name=$req->getParam("owner_full_name");
$owner_email=$req->getParam("owner_email");
$owner_password=$req->getParam("owner_password");
$owner_shop_contact=$req->getParam("owner_shop_contact");
$where="owner_id=$owner_id";

     $values=array("owner_full_name"=>$owner_full_name,
     "owner_email"=>$owner_email,
    "owner_password"=>$owner_password,"owner_shop_contact"=>$owner_shop_contact);
if(Shop_owners::updateOwner($owner_id,$values,$where)){
    echo json_encode(array("response"=>"ok","message"=>"succesffuly updated"));
}
else{
   echo json_encode(array("response"=>"failed","message"=>"failed to update shop owner"));
}
}



// update activate status
else if($req_type=="update_status"){
$req=Request::shouldBe("POST",array("shop_id","status"));
$shop_id=$req->getParam("shop_id");
$status=$req->getParam("status");
$where="shop_id=$shop_id";
     $values=array("status"=>$status);
if(Shop_owners::updateShop($shop_id,$values,$where)){
    echo json_encode(array("response"=>"ok","message"=>"succesffuly updated"));
}
else{
   echo json_encode(array("response"=>"failed","message"=>"failed to update shop owner"));
}
}

else if($req_type=="get_all_owners"){
     $req=Request::ShouldBe("POST",array("activation_key"));
        $activation_key=$req->getParam("activation_key");
        if($activation_key !=null){
    $where="activation_key='{$activation_key}'";
    $table="shop_owner";
    if($shop_owners=Shoppers::$DB->select($table,null,$where)){
            echo json_encode(array("response"=>"ok","students"=>$shop_owners));
    }
    else{
        echo json_encode(array("response"=>"error","message"=>"failed to fetch owners"));
    }
        }
        else{
 $where="";
    $table="shop_owner";
    if($shop_owners=Shoppers::$DB->select($table,null,$where)){
            echo json_encode(array("response"=>"ok","students"=>$shop_owners));
    }
    else{
        echo json_encode(array("response"=>"error","message"=>"failed to fetch owners"));
    }
        }
    
}


// to get certain shop gender type just supply shop_id
    else if($req_type=="get_all_shop_gender_type"){
       $req=Request::ShouldBe("POST",array("shop_id"));
       $shop_id=$req->getParam("shop_id");

   $where="shop_id=".$shop_id;
   $table="shop_gender_type";   
   $query="select * from all_gender_type inner join shop_gender_type on
    shop_gender_type.gender_type_id=all_gender_type.gender_type_id where shop_id=$shop_id";
     $result=Shoppers::$DB->link->query($query);

  $output=array();
  while($res=$result->fetch_assoc()){
 $rs['gender_type_id']=$res['gender_type_id'];
 $rs['gender_name']=$res['gender_name'];
 $rs['shop_id']=$res['shop_id'];

 $rs['gender_icon']=$res['gender_icon'];

 $output[]=$rs;
  }
  if($output!=null){
  echo json_encode(array("response"=>"ok","shop_gender_type_name"=>$output));
  }
  else{
      echo json_encode(array("response"=>"failed","message"=>"failed to fetch shop gender type names"));
 }
    }
// updating and registering shopviews
else if($req_type=="update_shop_view"){
     $req=Request::shouldBe("POST",array("shop_id"));
       $shop_id=$req->getParam("shop_id");
        $phone_id=$req->getParam("phone_id");
          $manufacturer=$req->getParam("manufacturer");
        $brand=$req->getParam("brand");
        $version=$req->getParam("version");
        $table_view="shop_view";
        $table_info="device_info";
        // register phone_details only if not exists
         $where_exist="shop_id='{$shop_id}' and phone_id='{$phone_id}'";
         
             
        if(Shoppers::$DB->select($table_info,null,$where_exist)){
 echo "exists";
        }
        else{
  $values=array("phone_id"=>$phone_id,"manufacturer"=>$manufacturer,"brand"=>$brand,"version"=>$version,"shop_id"=>$shop_id);
            // register details on the shop
                   if(Shoppers::$DB->insert($table_info,$values)){
                       //updating views
                         $sql="UPDATE `shop_view` SET `view`=view+1 WHERE shop_id='{$shop_id}'";
                          if($result=Shoppers::$DB->link->query($sql)){
    
          echo "yessss";  
        }
        else{
            echo "failed to update";
        }
         }
 
 }
 }
  


    //to delete shop gender type supply shop_id and gender_type_id
    else if($req_type=="delete_shop_gender_type"){
        $req=Request::shouldBe("POST",array("gender_type_id","shop_id"));
        $gender_type_id=$req->getParam("gender_type_id");
       $shop_id=$req->getParam("shop_id");
         Shop_owners::deleteShopGenderType($gender_type_id,$shop_id);
    }
    // to delete shop gender category name pass gender_type_id,shop_id and category_name_id
    else if($req_type=="delete_shop_category_name"){
        $req=Request::shouldBe("POST",array("gender_type_id","shop_id","category_name_id"));
        $gender_type_id=$req->getParam("gender_type_id");
       $shop_id=$req->getParam("shop_id");
        $category_name_id=$req->getParam("category_name_id");
        Shop_owners::deleteShopCategoryName($shop_id,$gender_type_id,$category_name_id);
    }
  // to delete shop gender type by master admin
  else if($req_type=="admin_delete_gender_type"){
        $req=Request::shouldBe("POST",array("gender_type_id"));
        $gender_type_id=$req->getParam("gender_type_id");
    
        Shop_owners::deleteGenderType($gender_type_id);
    }

// admin update gender type
else if($req_type=="update_gender_type"){
$req=Request::shouldBe("POST",array("gender_type_id","gender_name"));
$gender_type_id=$req->getParam("gender_type_id");
$gender_name=$req->getParam("gender_name");
$gender_type_icon=$req->getParam("gender_icon") !=null?$req->getParam("gender_icon"):null;
$where="gender_type_id=$gender_type_id";
$table="all_gender_type";

if($gender_type_icon !=null){
$gender_name_trimmed=preg_replace('/\s+/','',$gender_name.time().".png");
    $genderImage_path=$gender_folder.$gender_name_trimmed;
$full_gender_image_path=$host_folder.$genderImage_path;
   $image = base64_decode($gender_type_icon);
    $fp = fopen("../".$image, 'w');
    fwrite($fp, $genderImage_path);
    if(fclose($fp) ){
     echo "Image uploaded"."<br></br>";
    }else{
     echo "Error uploading image";
    }
     $values=array("gender_name"=>$gender_name,"gender_icon"=>$full_gender_image_path);

}
else{
     $values=array("gender_name"=>$gender_name);
}
if(Shoppers::$DB->update($table,$values,$where)){
    echo json_encode(array("response"=>"ok","message"=>"succesffuly updated"));
}
else{
   echo json_encode(array("response"=>"failed","message"=>"failed to update gender"));
}
}

//admin update gender_category 
else if($req_type=="update_category"){
$req=Request::shouldBe("POST",array("category_name_id","category_name","gender_type_id"));
$gender_type_id=$req->getParam("gender_type_id");
$category_name=$req->getParam("category_name");
$category_name_id=$req->getParam("category_name_id");
$category_name_icon=$req->getParam("category_name_icon") !=null?$req->getParam("category_name_icon"):null;
$where="category_name_id='{$category_name_id}' and gender_type_id='{$gender_type_id}'";
$table="all_gender_category_name";

if($category_name_icon !=null){
$category_name_trimmed=preg_replace('/\s+/','',$category_name.time().".png");
    $categoryImage_path=$category_folder.$category_name_trimmed;
$full_category_image_path=$host_folder.$categoryImage_path;
   $image = base64_decode($category_name_icon);
    $fp = fopen("../".$image, 'w');
    fwrite($fp, $categoryImage_path);
    if(fclose($fp) ){
     echo "Image uploaded"."<br></br>";
    }else{
     echo "Error uploading image";
    }
     $values=array("category_name"=>$category_name,"gender_type_id"=>$gender_type_id,"category_name_icon"=>$full_category_image_path);

}
else{
     $values=array("category_name"=>$category_name,"gender_type_id"=>$gender_type_id);
}
if(Shoppers::$DB->update($table,$values,$where)){
    echo json_encode(array("response"=>"ok","message"=>"succesffuly updated"));
}
else{
   echo json_encode(array("response"=>"failed","message"=>"failed to update category"));
}
}
         // To rate the shop
else if($req_type=="rate_shop"){
$req=Request::shouldBe("POST",array("shop_id","shop_rate"));
$shop_id=$req->getParam("shop_id");
$shop_rate=$req->getParam("shop_rate");
$where="shop_id=$shop_id";
$table="shop";
//$shop_name=$req->getParam("shop_name")!=null?$req->getParam("shop_name"):null;
     $values=array("rates"=>$shop_rate);
    
    $result=Shoppers::$DB->update($table,$values,$where); 
if($result){
    echo json_encode(array("response"=>"ok","message"=>"succesffuly updated"));
}
else{
   echo json_encode(array("response"=>"failed","message"=>"failed to update rates"));
}
         }

//  dont kno what it does// comment it
    else if($req_type=="get_category_type"){
        $req=Request::shouldBe("POST",array("gender_type_id","shop_id"));
        $gender_type_id=$req->getParam("gender_type_id");
       $shop_id=$req->getParam("shop_id");
       $where="gender_type_id=$gender_type_id and shop_id=$shop_id";
       $table="shop_gender_type";
       if($categories=Shoppers::$DB->select($table,null,$where)){
          echo json_encode(array("response"=>"ok","gender_types"=>$categories));
      }
      else{
          echo json_encode(array("response"=>"failed","messages"=>"failed to fetch categories"));
      }
    }
    // to get all gender types inorder to give shop_owner to choose 
    // u dont supply anything than specifying action get_all_gender_type
    else if($req_type=="get_all_gender_type"){
        Shop_owners::getGendersType();
    }
    // adding all the  gender type
    else if($req_type=="add_all_gender_type"){
    $req=Request::ShouldBe("POST",array("gender_type_name","gender_type_icon"));
        $gender_type_name=$req->getParam("gender_type_name");
        $gender_type_icon=$req->getParam("gender_type_icon")?$req->getParam("gender_type_icon"):null;
        $table="all_gender_type";
        $gender_name=preg_replace('/\s+/','',$gender_type_name.time().".png");
        $gender_path=$gender_folder.$gender_name;
        $full_gender_path=$host_folder.$gender_path;
        $values=array("gender_name"=>$gender_type_name,"gender_icon"=>$full_gender_path);
        $where="gender_name="."'$gender_type_name'";
        if($result=Shoppers::$DB->select($table,null,$where)){
            echo "gender type already exists";
        }
        else{
    if($result=Shoppers::$DB->insert($table,$values)){
    if($gender_type_icon!=null){
    $image = base64_decode($gender_type_icon);
    $fp = fopen("../".$gender_path, 'w');
    fwrite($fp, $image);
    if(fclose($fp)){
     echo "Image uploaded"."<br></br>";
    }else{
     echo "Error uploading image";
    }
            echo "gender type succesfully added";
        }
        else{
            echo "failed to add new gender type";
        }   
        }
    
    
        }
    
    }

// adding shop gender_views

 else if($req_type=="add_shop_gender_views"){
        $req=Request::ShouldBe("POST",array("shop_id","view","gender_type_id"));
        $shop_id=$req->getParam("shop_id");
        $gender_type_id=$req->getParam("gender_type_id");
        $view=$req->getParam("view");
        $table="shop_gender_views";
        // restricting multiple gender type
        $values=array("shop_id"=>$shop_id,"gender_type_id"=>$gender_type_id,"view"=>$view);
        $where="shop_id=$shop_id";
        if($result=Shoppers::$DB->insert($table,$values)){
            echo "succesfully added";
        }
        else{
            echo "failed";
        }   
        }
        // adding the shop_category_views
        else if($req_type=="add_shop_category_views"){
        $req=Request::ShouldBe("POST",array("shop_id","view","gender_type_id","category_name_id"));
        $shop_id=$req->getParam("shop_id");
        $category_name_id=$req->getParam("category_name_id");
        $gender_type_id=$req->getParam("gender_type_id");
        $view=$req->getParam("view");
        $table="shop_category_view";
        // restricting multiple gender type
        $values=array("shop_id"=>$shop_id,"gender_type_id"=>$gender_type_id,"category_name_id"=>$category_name_id,"view"=>$view);
        $where="shop_id=$shop_id";
        if($result=Shoppers::$DB->insert($table,$values)){
            echo "succesfully added";
        }
        else{
            echo "failed";
        }   
        }
        // to get shop_gender_views
        else if($req_type=="get_gender_type_views"){
        $req=Request::ShouldBe("POST",array("shop_id"));
        $shop_id=$req->getParam("shop_id");
        $table="shop_gender_views";
        // restricting multiple gender type
        $where="shop_id=$shop_id";
    
        $sql="select * from $table where shop_id='{$shop_id}'";
        if($result=Shoppers::$DB->link->query($sql)){
           //$rows=mysqli_num_rows($result);
            $output=array();
        while($res=$result->fetch_assoc()){
             $res['gender_type_id']=$res['gender_type_id'];
             $res['shop_id']=$res['shop_id'];
             $res['gender_viwe']=$res['view'];
             $output[]=$res;
        }
        if($output!=null){
       echo json_encode(array("response"=>"ok","shop_gender_views"=>$output));
        }
        else{
        echo "failed!! ";
        }
         }
         }      // getting shop category_views
         else if($req_type=="get_category_type_views"){
        $req=Request::ShouldBe("POST",array("shop_id","gender_type_id","category_name_id"));
        $shop_id=$req->getParam("shop_id");
        $gender_type_id=$req->getParam("gender_type_id");
       $category_name_id=$req->getParam("category_name_id");
        $table="shop_category_view";
        // restricting multiple gender type
    
    
        $sql="select * from $table where shop_id='{$shop_id}' and gender_type_id='{$gender_type_id}' and category_name_id='{$category_name_id}'";
        if($result=Shoppers::$DB->link->query($sql)){
$rows=mysqli_num_rows($result);
        echo  json_encode(array("response"=>"ok","shop_category_views"=>$rows));
        }
        else{
            echo  "failed";
        }
        }
        


// adding category name# admin
else if($req_type=="add_all_category_name"){
    $req=Request::ShouldBe("POST",array("category_name","category_name_icon","gender_type_id"));
        $category_name=$req->getParam("category_name");
        $gender_type_id=$req->getParam("gender_type_id");
        $category_name_icon=$req->getParam("category_name_icon")?$req->getParam("category_name_icon"):null;
        $table="all_gender_category_name";
        $category_name_trimmed=preg_replace('/\s+/','',$category_name.time().".png");
        $category_path=$gender_folder.$category_name_trimmed;
        $full_category_path=$host_folder.$category_path;
        $values=array("category_name"=>$category_name,"gender_type_id"=>$gender_type_id,"category_name_icon"=>$full_category_path);
        $where="category_name="."'$category_name' and gender_type_id="."'$gender_type_id'";
        if($result=Shoppers::$DB->select($table,null,$where)){
            echo "category type already exists";
        }
        else{
    if($result=Shoppers::$DB->insert($table,$values)){
    if($category_name_icon!=null){
    $image = base64_decode($category_name_icon);
    $fp = fopen("../".$category_path, 'w');
    fwrite($fp, $image);
    if(fclose($fp)){
     echo "Image uploaded"."<br></br>";
    }else{
     echo "Error uploading image";
    }
            echo "category name succesfully added";
        }
        else{
            echo "failed to add new gender type";
        }   
        }
    
    
        }
    
    }

    // to add shop_gender_type on certain shop supply shop_id,gender_type_id
     else if($req_type=="add_shop_gender_type"){
        $req=Request::ShouldBe("POST",array("shop_id","gender_type_id"));
        $shop_id=$req->getParam("shop_id");
        $gender_type_id=$req->getParam("gender_type_id");
        $table="shop_gender_type";
        // restricting multiple gender type
        $values=array("shop_id"=>$shop_id,"gender_type_id"=>$gender_type_id);
        $where="shop_id=$shop_id and gender_type_id=$gender_type_id";
        if($result=Shoppers::$DB->select($table,null,$where)){
            echo "shop gender type exists";
        }
        else{
      if($result=Shoppers::$DB->insert($table,$values)){
            echo "gender type succesfully added";
        }
        else{
            echo "failed to add gender type";
        }   
        }
    
        
      }
    // to get all category_name eg. all men category_name jus supply gender_type_id
      elseif ($req_type=="get_all_category_name"){
        $req=Request::ShouldBe("POST",array("gender_type_id"));
        $gender_type_id=$req->getParam("gender_type_id");
        $table="all_gender_category_name";
        $where="gender_type_id=$gender_type_id";
        if($all_gender_categories=Shoppers::$DB->select($table,null,$where)){
            echo  json_encode(array("response"=>"ok","message"=>$all_gender_categories));
        }
        else{
            echo  json_encode(array("response"=>"failed","message"=>"failed"));
        }
    }

// to get specific shop category name of certain gender type  supply shop_id, gender_type_id
     else if($req_type=="get_shop_category_name"){
     $req=Request::ShouldBe("POST",array("shop_id","gender_type_id"));
        $shop_id=$req->getParam("shop_id");
        $gender_type_id=$req->getParam("gender_type_id");
        $query= "select * from shop_category_name inner join all_gender_category_name on 
        shop_category_name.category_name_id=all_gender_category_name.category_name_id where shop_id=$shop_id and shop_category_name.gender_type_id=$gender_type_id";
     $result=Shoppers::$DB->link->query($query);

  $output=array();
  while($res=$result->fetch_assoc()){
  $rs['category_name']=$res['category_name'];
  $rs['category_name_id']=$res['category_name_id'];
  $rs['shop_id']=$res['shop_id'];
  $rs['category_name_icon']=$res['category_name_icon'];

  $output[]=$rs;
  }
  if($output!=null){
  echo json_encode(array("response"=>"ok","category_name"=>$output));
  }
  else{
      echo json_encode(array("response"=>"failed","message"=>"failed to fetch category names"));
 }
 }

 // to add categories name supply  gender_type_id,shop_id,and category_name_id
 else if($req_type=="add_category_name")
 {
        $req=Request::ShouldBe("POST",array("category_name_id","shop_id","gender_type_id"));
        $shop_id=$req->getParam("shop_id");
        $gender_type_id=$req->getParam("gender_type_id");
        $category_name_id=$req->getParam("category_name_id");
        $table="shop_category_name";
        $where="shop_id=$shop_id and category_name_id=$category_name_id and gender_type_id=$gender_type_id";
        $values=array("gender_type_id"=>$gender_type_id,"shop_id"=>$shop_id,"category_name_id"=>$category_name_id);
        if($result=Shoppers::$DB->select($table,null,$where)){
        echo "category exists";
         }
         else{
        if($result=Shoppers::$DB->insert($table,$values)){
               echo "category succesffuly added";
           }
           else{
               echo "failed to add category";
           }
         }
           
 }
 // to get all regions suppy state_id
 else if($req_type=="get_all_regions"){
    
     Shop_Region::getRegions();
 }
 // add comments
 else if($req_type=="add_comment"){
      $date_added=date("Y-m-d h:i:s");
         $req=Request::ShouldBe("POST",array("comment_name","user_id","trend_id","trend_gender_id","trend_category_id"));
     $user_id=$req->getParam("user_id");
     $trend_id=$req->getParam("trend_id");
         $trend_gender_id=$req->getParam("trend_gender_id");
             $trend_category_id=$req->getParam("trend_category_id");
     $comment_name=$req->getParam("comment_name");
    $table="comment";
    $values=array("comment_name"=>$comment_name,"date_added"=>$date_added,"user_id"=>$user_id,"trend_id"=>$trend_id,"trend_gender_id"=>$trend_gender_id,"trend_category_id"=>$trend_category_id);
    if( $result=Shoppers::$DB->insert($table,$values)){
        echo "succesfully inserted";
    }
    else{
        echo "failed";
    }
 }
 // register user to comment
  else if($req_type=="register_user"){
         $req=Request::ShouldBe("POST",array("user_name","user_phone","user_photo"));
     $user_name=$req->getParam("user_name");
     $user_phone=$req->getParam("user_phone");
     $user_photo=$req->getParam("user_photo");
    $user_photo_name=preg_replace('/\s+/','',$user_name.time().".png");  
$image_path=$user_folder.$user_photo_name;
$full_logo_path=$host_folder.$image_path;
    $table="user";
    $values=array("user_name"=>$user_name,"phone_number"=>$user_phone,"photo"=>$full_logo_path);
    if( $inserted_id=Shoppers::$DB->insert($table,$values)){
        // echo "succesfully registered";
              echo json_encode(array("response"=>"ok","user_id"=>$inserted_id));
    }
    else{
        echo "failed";
    }
        $image = base64_decode($user_photo);
    $fp = fopen("../".$image_path, 'w');
    fwrite($fp, $image);
    if(fclose($fp)){
     //echo "Image uploaded"."<br></br>";
    }else{
     echo "Error uploading image";
    }

 }
 // get comments
 else if($req_type=="get_comment"){
     $req=Request::ShouldBe("POST",array("trend_gender_id","trend_category_id","trend_id"));
     $trend_gender_id=$req->getParam("trend_gender_id");
      $trend_category_id=$req->getParam("trend_category_id");
       $trend_id=$req->getParam("trend_id");

$sql= "SELECT user.user_id,comment.comment_id,user_name,comment_name,date_added,photo from comment inner join  user on user.user_id=comment.user_id
 WHERE trend_gender_id='{$trend_gender_id}' and trend_category_id='{$trend_category_id}' and trend_id='{$trend_id}' ORDER BY comment.date_added ASC";
if($result=Shoppers::$DB->link->query($sql)){
            $output=array();
        while($res=$result->fetch_assoc()){
             $res['user_name']=$res['user_name'];
             $res['comment_name'] =$res['comment_name'];
             $res['date_added'] =$res['date_added'];
             $res['photo']  =$res['photo'];
             $output[]=$res;
        }
        if($output!=null){
       echo json_encode(array("response"=>"ok","comments"=>$output));
        }
        else{
        echo "no comment yet!!!";
        }
    
    }
    else{
        echo json_encode(array("response"=>"error","message"=>"failed to fetch comments"));
    }

 }

// delete the comment
 else if($req_type=="delete_comment"){
     $req=Request::ShouldBe("POST",array("user_id","comment_id","trend_gender_id","trend_category_id","trend_id"));
     $user_id=$req->getParam("user_id");
     $comment_id=$req->getParam("comment_id");
     $trend_gender_id=$req->getParam("trend_gender_id");
     $trend_category_id=$req->getParam("trend_category_id");
     $trend_id=$req->getParam("trend_id");
     $table="comment";
     $where="user_id='{$user_id}' and comment_id='{$comment_id}' and trend_gender_id='{$trend_gender_id}' and trend_category_id='{$trend_category_id}' and trend_id='{$trend_id}'";
     if(Shoppers::$DB->delete($table,$where)){
         echo "deleted";
     }
     else{
         echo "failed to delete";
     }
 }
 // get specific region
 else if($req_type=="get_region"){
     $req=Request::ShouldBe("POST",array("region_id"));
     $region_id=$req->getParam("region_id");
     Shop_Region::getRegion($region_id);
 }
 else if($req_type=="get_districts"){
     $req=Request::ShouldBe("POST",array("region_id"));
     $region_id=$req->getParam("region_id");
     Shop_District::getDistricts($region_id);
 }
 // get specific district
  else if($req_type=="get_district"){
     $req=Request::ShouldBe("POST",array("district_id"));
     $district_id=$req->getParam("district_id");
     Shop_District::getDistrict($district_id);
 }
  // to login shop_owners store shop_owners id as the shop_id on your session
 else if($req_type=="login_shop_owner")
 {
      $req=Request::ShouldBe("POST",array("owner_shop_contact","owner_password","owner_activation_key"));
      $owner_shop_contact= $req->getParam("owner_shop_contact");
      $owner_password=$req->getParam("owner_password");
      $owner_activation_key= $req->getParam("owner_activation_key");
      Shop_owners::loginOwner($owner_shop_contact,$owner_password,$owner_activation_key);
 }
  // to login admin in the system
 else if($req_type=="login_admin")
 {
      $req=Request::ShouldBe("POST",array("username","password"));
      $username= $req->getParam("username");
      $password=$req->getParam("password");
      Shop_owners::adminLoginOwner($username,$password);
 }
 
 // to search shops by regions
 else if($req_type=="search_by_region")
 {
      $req=Request::ShouldBe("POST",array("region_id"));
      $region_id= $req->getParam("region_id");
$sql= "SELECT shop_name,shop.shop_id, shop_description,shop_logo, district.district_id, district_name, region_name, region.region_id, street_name,view
FROM shop
INNER JOIN district ON shop.district_id = district.district_id
INNER JOIN region ON region.region_id = district.region_id inner join shop_view on shop_view.shop_id=shop.shop_id 
WHERE region.region_id ='{$region_id}'and status=1";
if($result=Shoppers::$DB->link->query($sql)){
            $output=array();
        while($res=$result->fetch_assoc()){
             $res['shop_id']=$res['shop_id'];
             $res['shop_name']=$res['shop_name'];
             $res['shop_description']  =$res['shop_description'];
             $res['shop_logo'] =$res['shop_logo'];
             $res['region_name'] =$res['region_name'];
             $res['region_id'] =$res['region_id'];
             $res['district_name'] =$res['district_name'];
             $res['district_id'] =$res['district_id'];
             $res['street_name'] =$res['street_name'];
             $res['shop_view'] =$res['view'];
             $output[]=$res;
        }
        if($output!=null){
       echo json_encode(array("response"=>"ok","shop"=>$output));
        }
        else{
        echo "no shop on your region yet!!! ";
        }
    
    }
    else{
        echo json_encode(array("response"=>"error","message"=>"failed to fetch shops"));
    }
}

// search shop by district
 else if($req_type=="search_by_district")
 {
      $req=Request::ShouldBe("POST",array("district_id"));
      $district_id= $req->getParam("district_id");
      
$sql= "SELECT shop_name,shop.shop_id, shop_description,shop_logo, district.district_id, district_name, region_name, region.region_id, street_name,view
FROM shop
INNER JOIN district ON shop.district_id = district.district_id
INNER JOIN region ON region.region_id = district.region_id inner join shop_view on shop_view.shop_id=shop.shop_id
WHERE district.district_id ='{$district_id}' and status=1";
if($result=Shoppers::$DB->link->query($sql)){
        $output=array();
        while($res=$result->fetch_assoc()){
             
              $res['shop_id']=$res['shop_id'];
             $res['shop_name']=$res['shop_name'];
             $res['shop_description']  =$res['shop_description'];
             $res['shop_logo'] =$res['shop_logo'];
             $res['region_name'] =$res['region_name'];
             $res['region_id'] =$res['region_id'];
             $res['district_name'] =$res['district_name'];
             $res['district_id'] =$res['district_id'];
             $res['street_name'] =$res['street_name'];
             $res['shop_view'] =$res['view'];
             
             $output[]=$res;
        }
    if($output!=null){
       echo json_encode(array("response"=>"ok","shop"=>$output));
        }
        else{
        echo "no shop on your district yet!!! ";
        }
    }
    else{
        echo json_encode(array("response"=>"error","message"=>"failed to fetch shops"));
    }
}
