<?php
require_once('../controllers/Shop_owners.php');
 require_once('../controllers/Database.php');
 require_once('../controllers/Request.php');
 require_once('../controllers/Shop_Region.php');
 require_once('../controllers/Shop_District.php');
 $request=Request::shouldBe("POST",array("action"));
 $req_type=$request->getParam("action");
 date_default_timezone_set('Africa/Nairobi');
 $date_added=date("Y-m-d H:i:s");
 $host_folder="http://foodly.pe.hu/shopperz/shoppers_hype/";
 $category_folder="images/trends_category_icon/";
  $trends_folder="images/trends_post/";

// posting trends fashion 
 if($req_type=="add_trends_post"){
 $req =Request::shouldBe("POST",array("fashion_trend_name","fashion_trend_description","fashion_trend_price","fashion_phone_number","fashion_trend_gender_id","quantity","color","img_name1","img_name2","img_name3"));
 $image_name1=$req->getParam("img_name1");
 $image_name2=$req->getParam("img_name2");
 $image_name3=$req->getParam("img_name3");
 $color=$req->getParam("color");
 $quantity=$req->getParam("quantity");
$size=$req->getParam("size");
 $gender_type_id=$req->getParam("fashion_trend_gender_id"); 
 $category_id=$req->getParam("trend_category_id");
 $trend_name=$req->getParam("fashion_trend_name");
 $trend_description=$req->getParam("fashion_trend_description");
 $trend_price=$req->getParam("fashion_trend_price");
 $trend_contact=$req->getParam("fashion_phone_number");
 $imageName=$trend_name.time().".png";
// restrictin multiple post of the product
$table="fashion_trends";
$where="trend_name='$trend_name' and  trend_description='$trend_description'
 and trend_price='$trend_price' and trend_gender_id='$gender_type_id' and trend_category_id='$category_id'";
 if($result=Shoppers::$DB->select($table,null,$where)){
	  echo "post with this details already exists please change post details ";
 }
 else{
$values=array("trend_name"=>$trend_name,"trend_description"=>$trend_description,
"price"=>$trend_price,"phone_number"=>$trend_contact,"trend_description"=>$trend_description,"quantity"=>$quantity,"color"=>$color,
"size"=>$size,"trend_category_id"=>$category_id,"trend_gender_id"=>$gender_type_id,"date_added"=>$date_added);
Shoppers::$DB->start();
$inserted_id=Shoppers::$DB->insert($table,$values);
$table2="trend_images";

 $imageName1=preg_replace('/\s+/','',$trend_name."1".".png");
 $imageName2=preg_replace('/\s+/','',$trend_name."2".".png");
 $imageName3=preg_replace('/\s+/','',$trend_name."3".".png");
$image_path1=$trends_folder."/".$imageName1;
$full_image_path1=$host_folder.$image_path1;
   $image_name11 = base64_decode($image_name1);
	$fp = fopen("../".$image_path1, 'w');
	fwrite($fp, $image_name11);
	if(fclose($fp)){
	 echo "Image uploaded"."<br></br>";
	}else{
	 echo "Error uploading image";
	}

    $image_path2=$trends_folder."/".$imageName2;
  $full_image_path2=$host_folder.$image_path2;
   $image_name22 = base64_decode($image_name2);
	$fp = fopen("../".$image_path2, 'w');
	fwrite($fp, $image_name22);
	if(fclose($fp)){
	 echo "Image uploaded"."<br></br>";
	}else{
	 echo "Error uploading image";
	}
    
$image_path3=$trends_folder."/".$imageName3;
$full_image_path3=$host_folder.$image_path3;
   $image_name33 = base64_decode($image_name3);
	$fp = fopen("../".$image_path3, 'w');
    
	fwrite($fp, $image_name33);
	if(fclose($fp) ){
	 echo "Image uploaded"."<br></br>";
	}else{
	 echo "Error uploading image";
	}
     $values_image=array("image_name"=>$full_image_path1,"image_name2"=>$full_image_path2,"image_name3"=>$full_image_path3,"trend_id"=>$inserted_id);
      
if(Shoppers::$DB->insert($table2,$values_image)){
   
    Shoppers::$DB->commit();
	echo "post uploaded succesfully";
    
}

else{
    Shoppers::$DB->rollback();
    echo "failed to add images";
}

 }
 }
 // getting all trends post\
  
 else if($req_type=="get_all_trends_categories_post"){
	 $req=Request::shouldBe("POST",array("trend_category_id","trend_gender_id"));
	 $trend_gender_id=$req->getParam("trend_gender_id");
	 $category_name_id=$req->getParam("trend_category_id");
	 $sql="select * from fashion_trends
INNER JOIN trend_images ON fashion_trends.trend_id = trend_images.trend_id 
where fashion_trends.trend_gender_id=$trend_gender_id and fashion_trends.trend_category_id=$category_name_id
";
$result=Shoppers::$DB->link->query($sql);
  $output=array();
  while($res=$result->fetch_assoc()){
 $rs['trend_id']=$res['trend_id'];	  
 $rs['trend_gender_id']=$res['trend_gender_id'];
 $rs['trend_category_id']=$res['trend_category_id'];
 $rs['trend_name']=$res['trend_name'];
 $rs['trend_price']=$res['price'];
 $rs['phone_number']=$res['phone_number'];
 $rs['trend_description']=$res['trend_description'];
 $rs['quantity']=$res['quantity'];
 $rs['color']=$res['color'];
 $rs['size']=$res['size'];
 $rs['image_name1']=$res['image_name'];
 $rs['image_name2']=$res['image_name2'];
 $rs['image_name3']=$res['image_name3'];
 $rs['date_added']=$res['date_added'];

 $output[]=$rs;
  }
  if($output!=null){
  echo json_encode(array("response"=>"ok","fashion_post"=>$output));
  }
  else{
	  echo json_encode(array("response"=>"failed","message"=>"failed to fetch trends category post"));
 }
 }
 // to get specific trend post
  else if($req_type=="get_trends_categories_post"){
	 $req=Request::shouldBe("POST",array("trend_gender_id","trend_id","trend_category_id"));
	 $trend_gender_id=$req->getParam("trend_gender_id");
	 $trend_category_id=$req->getParam("trend_category_id");
     	 $trend_id=$req->getParam("trend_id");
	 $sql="SELECT *
FROM fashion_trends
INNER JOIN trend_images ON fashion_trends.trend_id = trend_images.trend_id
WHERE fashion_trends.trend_category_id =$trend_category_id
and  fashion_trends.trend_id=$trend_id
AND  fashion_trends.trend_gender_id =
$trend_gender_id";
$result=Shoppers::$DB->link->query($sql);
  $output=array();
  while($res=$result->fetch_assoc()){
 $rs['trend_id']=$res['trend_id'];	  
 $rs['trend_gender_id']=$res['trend_gender_id'];
 $rs['trend_category_id']=$res['trend_category_id'];
 $rs['trend_name']=$res['trend_name'];
 $rs['trend_price']=$res['price'];
 $rs['phone_number']=$res['phone_number'];
 $rs['trend_description']=$res['trend_description'];
 $rs['image_name1']=$res['image_name'];
 $rs['image_name2']=$res['image_name2'];
 $rs['image_name3']=$res['image_name3'];
 $rs['date_added']=$res['date_added'];
 $output[]=$rs;
  }
  if($output!=null){
  echo json_encode(array("response"=>"ok","shop_category_post"=>$output));
  }
  else{
	  echo json_encode(array("response"=>"failed","message"=>"failed to fetch trends category post"));
 }
 }

 //to add all gender category // admin
else if($req_type=="add_trends_category"){
	$req=Request::ShouldBe("POST",array("category_name","g_id","c_icon"));
		$category_name=$req->getParam("category_name");
		$gender_type_id=$req->getParam("g_id");
		$category_name_icon=$req->getParam("c_icon")?$req->getParam("c_icon"):null;
		$table="trend_category";
		$category_name_trimmed=preg_replace('/\s+/','',$category_name.time().".png");
		$category_path=$category_folder.$category_name_trimmed;
        $full_category_path=$host_folder.$category_path;
		$values=array("category_name"=>$category_name,"trend_gender_id"=>$gender_type_id,"category_icon"=>$full_category_path);
		$where="category_name="."'$category_name' and trend_gender_id="."'$gender_type_id'";
		if($result=Shoppers::$DB->select($table,null,$where)){
	  		echo "category type already exists";
		}
		else{
    if($result=Shoppers::$DB->insert($table,$values)){
	echo "category name succesfully added";
		}
		if($category_name_icon!=null){
$image = base64_decode($category_name_icon);
	$fp = fopen("../".$category_path, 'w');
	fwrite($fp, $image);
	if(fclose($fp)){
	// echo "Image uploaded"."<br></br>";
	}else{
	 echo "Error uploading image";
	}

		}
	
    		
    
}}

    //to get all_trend_gender_categories
else if($req_type=="get_trends_category"){
	$req=Request::ShouldBe("POST",array("gender_type_id"));
	
		$gender_type_id=$req->getParam("gender_type_id");
		$table="trend_category";
		$where="trend_gender_id=$gender_type_id";
		if($all_gender_categories=Shoppers::$DB->select($table,null,$where)){
			echo  json_encode(array("response"=>"ok","message"=>$all_gender_categories));
		}
		else{
			echo  json_encode(array("response"=>"failed","message"=>"failed"));
		}
	}
        //to get specific_trend_gender_categories
else if($req_type=="get_specific_trends_category"){
	$req=Request::ShouldBe("POST",array("trend_gender_id","trend_category_id"));
	
		$gender_type_id=$req->getParam("trend_gender_id");
        		$trend_category_id=$req->getParam("trend_category_id");
		$table="trend_category";
		$where="trend_gender_id=$gender_type_id and trend_category_id=$trend_category_id";
		if($all_gender_categories=Shoppers::$DB->select($table,null,$where)){
			echo  json_encode(array("response"=>"ok","message"=>$all_gender_categories));
		}
		else{
			echo  json_encode(array("response"=>"failed","message"=>"failed"));
		}
	}
    // to add trends gender 
	else if($req_type=="add_trends_gender"){
	$req=Request::ShouldBe("POST",array("gender_type_name"));
	
		$gender_type_name=$req->getParam("gender_type_name");
		$table="trend_gender_category";
		$values=array("gender_name"=>$gender_type_name);
        
		if($result=Shoppers::$DB->insert($table,$values,null)){
			echo  json_encode(array("response"=>"ok","message"=>"gender added"));
		}
		else{
			echo  json_encode(array("response"=>"failed","message"=>"failed"));
		}
	}
    
