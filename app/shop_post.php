<?php
 require_once('../controllers/Shop_owners.php');
 require_once('../controllers/Database.php');
 require_once('../controllers/Request.php');
 require_once('../controllers/Shop_Region.php');
 require_once('../controllers/Shop_District.php');

 $request=Request::shouldBe("POST",array("action"));
 $req_type=$request->getParam("action");
date_default_timezone_set('Africa/Nairobi');
 $date_added=date("Y-m-d H:i:s");
 $host_folder="http://foodly.pe.hu/shopperz/shoppers_hype/";
 $images_folder="images/product_posts";

// posting shop product 
 if($req_type=="add_product"){
 $req =Request::shouldBe("POST",array("product_name","product_description","product_price","product_size","category_name_id","gender_type_id","shop_id","image_name1","image_name2","image_name3"));
 $image_name1=$req->getParam("image_name1");
 $image_name2=$req->getParam("image_name2");
 $image_name3=$req->getParam("image_name3");
 $category_name_id=$req->getParam("category_name_id");
 $gender_type_id=$req->getParam("gender_type_id"); 
 $shop_id=$req->getParam("shop_id");
 $product_name=$req->getParam("product_name");
 $product_description=$req->getParam("product_description");
 $product_price=$req->getParam("product_price");
 $product_size=$req->getParam("product_size");
 $product_quantity=$req->getParam("product_quantity");
 $product_color=$req->getParam("product_color");
 $category_name_id=$req->getParam("category_name_id");
 $gender_type_id=$req->getParam("gender_type_id");
 $shop_id=$req->getParam("shop_id");
 $imageName=$product_name.time().".png";
// restrictin multiple post of the product
$table="product";
$where="product_name='$product_name' and  product_description='$product_description'
 and product_size='$product_size' and gender_type_id='$gender_type_id' and shop_id='$shop_id' and category_name_id='$category_name_id'";
 if($result=Shoppers::$DB->select($table,null,$where)){
	  echo "post with this details already exists please change post details ";
 }
 else{
$values=array("product_name"=>$product_name,"product_description"=>$product_description,
"product_price"=>$product_price,"product_size"=>$product_size,"quantity"=>$product_quantity,"color"=>$product_color,"product_description"=>$product_description,
"category_name_id"=>$category_name_id,"gender_type_id"=>$gender_type_id,
"shop_id"=>$shop_id,"date_added"=>$date_added);
Shoppers::$DB->start();
$inserted_id=Shoppers::$DB->insert($table,$values);

$table2="product_image";
 $values_image2=array("image_name"=>$image_name2,"product_id"=>$inserted_id);
 $values_image3=array("image_name"=>$image_name3,"product_id"=>$inserted_id);
 $imageName1=preg_replace('/\s+/','',$product_name."1".".png");
 $imageName2=preg_replace('/\s+/','',$product_name."2".".png");
 $imageName3=preg_replace('/\s+/','',$product_name."3".".png");
$image_path1=$images_folder."/".$imageName1;
$full_image_path1=$host_folder.$image_path1;
   $image_name11 = base64_decode($image_name1);
	$fp = fopen("../".$image_path1, 'w');
	fwrite($fp, $image_name11);
	if(fclose($fp)){
	 echo "Image uploaded"."<br></br>";
	}else{
	 echo "Error uploading image";
	}

    $image_path2=$images_folder."/".$imageName2;
$full_image_path2=$host_folder.$image_path2;
   $image_name22 = base64_decode($image_name2);
	$fp = fopen("../".$image_path2, 'w');
	fwrite($fp, $image_name22);
	if(fclose($fp)){
	 echo "Image uploaded"."<br></br>";
	}else{
	 echo "Error uploading image";
	}
    
$image_path3=$images_folder."/".$imageName3;
$full_image_path3=$host_folder.$image_path3;
   $image_name33 = base64_decode($image_name3);
	$fp = fopen("../".$image_path3, 'w');
    
	fwrite($fp, $image_name33);
	if(fclose($fp) ){
	 echo "Image uploaded"."<br></br>";
	}else{
	 echo "Error uploading image";
	}
     $values_image=array("image_name"=>$full_image_path1,"image_name2"=>$full_image_path2,"image_name3"=>$full_image_path3,"product_id"=>$inserted_id);
      
if(Shoppers::$DB->insert($table2,$values_image)){
   
    Shoppers::$DB->commit();
	echo "post uploaded succesfully";
    
}

else{
    Shoppers::$DB->rollback();
    echo "failed to add images";
}

 }
 }

 // eg. to get all shop post eg.men jeans of zizzoo 
  else if($req_type=="get_all_categories_post"){
	 $req=Request::shouldBe("POST",array("gender_type_id","shop_id","category_name_id"));
     $shop_id=$req->getParam("shop_id");
	 $gender_type_id=$req->getParam("gender_type_id");
	 $category_name_id=$req->getParam("category_name_id");
	 $sql="SELECT *
FROM product
INNER JOIN product_image ON product.product_id = product_image.product_id
WHERE category_name_id =$category_name_id
AND gender_type_id =$gender_type_id
AND shop_id =$shop_id";
$result=Shoppers::$DB->link->query($sql);

  $output=array();
  while($res=$result->fetch_assoc()){
 $rs['post_id']=$res['id'];	  
 $rs['gender_type_id']=$res['gender_type_id'];
 $rs['shop_id']=$res['shop_id'];
 $rs['category_name_id']=$res['category_name_id'];
 $rs['product_name']=$res['product_name'];
 $rs['product_quantity']=$res['quantity'];
 $rs['product_color']=$res['color'];
 $rs['product_price']=$res['product_price'];
 $rs['product_size']=$res['product_size'];
 $rs['product_description']=$res['product_description'];
 $rs['image_name1']=$res['image_name'];
 $rs['image_name2']=$res['image_name2'];
 $rs['image_name3']=$res['image_name3'];
 $rs['date_added']=$res['date_added'];
 $output[]=$rs;
  }
  if($output!=null){
  echo json_encode(array("response"=>"ok","shop_category_post"=>$output));
  }
  else{
	  echo json_encode(array("response"=>"failed","message"=>"failed to fetch shop category post"));
 }
 }
else if($req_type=="get_all_shop_post"){
	 $req=Request::shouldBe("POST",array("shop_id"));
     $shop_id=$req->getParam("shop_id");
	 $sql="
select * from product inner join product_image on 
product_image.product_id=product.product_id where shop_id=$shop_id";
 $result=Shoppers::$DB->link->query($sql);
  $output=array();
  while($res=$result->fetch_assoc()){
	  
  $rs['product_name']=$res['product_name'];	  
  $rs['product_description']=$res['product_description'];
  $rs['shop_id']=$res['product_price'];
 $rs['category_name_id']=$res['product_size'];
 $rs['product_color']=$res['color'];	  
 $rs['product_quantity']=$res['quantity'];	  
 $rs['product_name']=$res['category_name_id'];
 $rs['product_price']=$res['date_added'];
 $rs['product_size']=$res['gender_type_id'];
 $rs['image_name1']=$res['image_name'];
 $rs['image_name2']=$res['image_name2'];
 $rs['image_name3']=$res['image_name3'];
 $output[]=$rs;
  }
  if($output!=null){
  echo json_encode(array("response"=>"ok","shop_all_post"=>$output));
  }
  else{
	  echo json_encode(array("response"=>"failed","message"=>"failed to fetch shop all post"));
 }
	 
 }
 // to get particular post of the shop . provide post_id
elseif($req_type=="get_shop_post"){
	 $req=Request::shouldBe("POST",array("product_id","shop_id","category_name_id","gender_type_id"));
     $post_id=$req->getParam("product_id");
	 $product_quantity=$req->getParam("product_quantity");
	 $product_color=$req->getParam("product_color");
	 $gender_type_id=$req->getParam("gender_typ_id");
	 $category_name_id=$req->getParam("category_name_id");
	 $shop_id=$req->getParam("shop_id");
	 $sql="SELECT product_name, product_price,quantity,color, quantity,color,product_description, product_size, image_name, image_name2, image_name3, date_added
FROM product
INNER JOIN product_image ON product.product_id ='{$post_id}'
AND product_image.product_id ='{$post_id}'
WHERE shop_id ='{$shop_id}'
AND category_name_id ='{$category_name_id}'
AND gender_type_id ='{$category_name_id}'";
	   $output=array();
	 $result=Shoppers::$DB->link->query($sql);
	
	
  while($res=$result->fetch_assoc()){
 $rs['product_name']=$res['product_name'];
 $rs['product_price']=$res['product_price'];
 $rs['product_quantity']=$res['quantity'];
 $rs['product_color']=$res['color'];
 $rs['product_size']=$res['product_size'];
 $rs['product_description']=$res['product_description'];
 $rs['image_name1']=$res['image_name'];
 $rs['image_name2']=$res['image_name2'];
 $rs['image_name3']=$res['image_name3'];
 $rs['date_added']=$res['date_added'];
 $output[]=$rs;
  }
  if($output!=null){
  echo json_encode(array("response"=>"ok","shop_category_post"=>$output));
  }
  else{
	  echo json_encode(array("response"=>"failed","message"=>"failed to fetch shop category post"));
 }
 }
 // to delete shop product postshop
 else if($req_type=="delete_shop_post")
 {
	 $req=Request::shouldBe("POST",array("shop_id","gender_type_id","category_name_id","product_id"));
	 $gender_type_id=$req->getParam("gender_type_id");
	 $product_id=$req->getParam("product_id");
	 $category_name_id=$req->getParam("category_name_id");
	 $shop_id=$req->getParam("shop_id");	 
	 $sql="DELETE product, product_image from product inner join product_image
	  on product.product_id= $product_id and product_image.product_id=$product_id";
	  if($result=Shoppers::$DB->link->query($sql)){
		  echo "post has been deleted";
	  }
	  else{
		  echo "failed to delete the post";
	  }

 }

 // to update the shop_post
else if($req_type=="update_shop_post"){
$req =Request::shouldBe("POST",array("product_id","product_name","product_description","product_price","product_size","category_name_id","gender_type_id","shop_id","image_name1","image_name2","image_name3"));
 $image_name1=$req->getParam("image_name1");
 $image_name2=$req->getParam("image_name2");
 $image_name3=$req->getParam("image_name3");
 $category_name_id=$req->getParam("category_name_id");
 $gender_type_id=$req->getParam("gender_type_id");
  $product_name=$req->getParam("product_name");
$product_description=$req->getParam("product_description");
$product_price=$req->getParam("product_price");
$product_color=$req->getParam("product_color");
$product_quantity=$req->getParam("product_quantity");
$product_size=$req->getParam("product_size");
$category_name_id=$req->getParam("category_name_id");
$gender_type_id=$req->getParam("gender_type_id");
$shop_id=$req->getParam("shop_id");
$post_id=$req->getParam("product_id");

 $imageName1=preg_replace('/\s+/','',$product_name."1".".png");
 $imageName2=preg_replace('/\s+/','',$product_name."2".".png");
 $imageName3=preg_replace('/\s+/','',$product_name."3".".png");


  $column="product";
  $column2="product_image";
     if($image_name1!=null && $image_name2!=null && $image_name3!=null){
      // write image_path
   $image_path1=$images_folder."/".$imageName1;
   $full_image_path1=$host_folder.$image_path1;
  $image_name11 = base64_decode($image_name1);
	$fp = fopen("../".$image_path1, 'w');
	fwrite($fp, $image_name11);
	if(fclose($fp)){
	 echo "Image uploaded"."<br></br>";
	}else{
	 echo "Error uploading image";
	}

   $image_path2=$images_folder."/".$imageName2;
   $full_image_path2=$host_folder.$image_path2;

   $image_name22= base64_decode($image_name2);
	$fp = fopen("../".$image_path2, 'w');
	fwrite($fp, $image_name22);
	if(fclose($fp)){
	 echo "Image uploaded"."<br></br>";
	}else{
	 echo "Error uploading image";
	}
    
$image_path3=$images_folder."/".$imageName3;
$full_image_path3=$host_folder.$image_path3;
   $image_name33 = base64_decode($image_name3);
	$fp = fopen("../".$image_path3, 'w');
    
	fwrite($fp, $image_name33);
	if(fclose($fp) ){
	 echo "Image uploaded"."<br></br>";
	}else{
	 echo "Error uploading image";
	 }
  $sql_all="UPDATE  product,product_image set product.product_name='$product_name',product.product_price='$product_price',
     color='$product_color',quantity='$product_quantity',
	 product.product_size='$product_size',product.category_name_id=$category_name_id,product.gender_type_id=$gender_type_id,
     product.shop_id=$shop_id,product_image.image_name='$full_image_path1',product_image.image_name2='$full_image_path2',
	 product_image.image_name3='$full_image_path3' WHERE product.product_id=$post_id AND product_image.product_id=$post_id";
    if(Shoppers::$DB->link->query($sql_all)){
		echo "succesffuly updated";
	}
	else{
		echo "failed to update";
	}
	 }

	 elseif ($image_name1!=null && $image_name2!=null){
   $image_path1=$images_folder."/".$imageName1;
  $full_image_path1=$host_folder.$image_path1;
   $image_name11 = base64_decode($image_name1);
	$fp = fopen("../".$image_path1, 'w');
	fwrite($fp, $image_name11);
	if(fclose($fp)){
	 echo "Image uploaded"."<br></br>";
	}else{
	 echo "Error uploading image";
	}

   $image_path2=$images_folder."/".$imageName2;
   $full_image_path2=$host_folder.$image_path2;
  $image_name22 = base64_decode($image_name2);
	$fp = fopen("../".$image_path2, 'w');
	fwrite($fp, $image_name22);
	if(fclose($fp)){
	 echo "Image uploaded"."<br></br>";
	}else{
	 echo "Error uploading image";
	}
  
 $sql_all="UPDATE  product,product_image set product.product_name='$product_name',product.product_price='$product_price',
 color='$product_color',quantity='$product_quantity',
	 product.product_size='$product_size',product.category_name_id=$category_name_id,product.gender_type_id=$gender_type_id,
     product.shop_id=$shop_id,product_image.image_name='$full_image_path1',product_image.image_name2='$full_image_path2'
	 WHERE product.product_id=$post_id AND product_image.product_id=$post_id";
    if(Shoppers::$DB->link->query($sql_all)){
		echo "succesffuly updated";
	}
	else{
		echo "failed to update";
	}

	 }

	  elseif ($image_name1!=null && $image_name3!=null){
  $image_path1=$images_folder."/".$imageName1;
  $full_image_path1=$host_folder.$image_path1;
  $image_name11 = base64_decode($image_name1);
	$fp = fopen("../".$image_path1, 'w');
	fwrite($fp, $image_name11);
	if(fclose($fp)){
	 echo "Image uploaded"."<br></br>";
	}else{
	 echo "Error uploading image";
	}

   $image_path3=$images_folder."/".$imageName3;
$full_image_path3=$host_folder.$image_path3;
   $image_name33 = base64_decode($image_name3);
	$fp = fopen("../".$image_path3, 'w');
	fwrite($fp, $image_name33);
	if(fclose($fp)){
	 echo "Image uploaded"."<br></br>";
	}else{
	 echo "Error uploading image";
	}
  
   $sql_all="UPDATE  product,product_image set product.product_name='$product_name',product.product_price='$product_price',
   color='$product_color',quantity='$product_quantity',
	 product.product_size='$product_size',product.category_name_id=$category_name_id,product.gender_type_id=$gender_type_id,
     product.shop_id=$shop_id,product_image.image_name='$full_image_path1',product_image.image_name3='$full_image_path3'
	 WHERE product.product_id=$post_id AND product_image.product_id=$post_id";
    if(Shoppers::$DB->link->query($sql_all)){
		echo "succesffuly updated";
	}
	else{
		echo "failed to update";
	}

	 }
elseif ($image_name2!=null && $image_name3!=null){
   $image_path2=$images_folder.$imageName2;
   $full_image_path2=$host_folder.$image_path2;
   $image_name22 = base64_decode($image_name2);
   $fp = fopen("../".$image_path2, 'w');
   fwrite($fp, $image_name22);
   if(fclose($fp)){
	echo "Image uploaded"."<br></br>";
	}else{
	 echo "Error uploading image";
	}

   $image_path3=$images_folder."/".$imageName3;
  $full_image_path3=$host_folder.$image_path3;
   $image_name33 = base64_decode($image_name3);
   $fp = fopen("../".$image_path3, 'w');
	fwrite($fp, $image_name33);
	if(fclose($fp)){
	 echo "Image uploaded"."<br></br>";
	}else{
	 echo "Error uploading image";
	}

     $sql_all="UPDATE  product,product_image set product.product_name='$product_name',product.product_price='$product_price',
	 color='$product_color',quantity='$product_quantity',
	 product.product_size='$product_size',product.category_name_id=$category_name_id,product.gender_type_id=$gender_type_id,
     product.shop_id=$shop_id,product_image.image_name2='$full_image_path2',product_image.image_name3='$full_image_path3'
	 WHERE product.product_id=$post_id AND product_image.product_id=$post_id";
     if(Shoppers::$DB->link->query($sql_all)){
		echo "succesffuly updated";
	}
 	else{
		echo "failed to update";
	}

	 }

   else if($image_name1!=null){
   $image_path1=$images_folder."/".$imageName1;
$full_image_path1=$host_folder.$image_path1;
   $image_name11 = base64_decode($image_name1);
  $fp = fopen("../".$image_path1, 'w');
	fwrite($fp, $image_name11);
	if(fclose($fp)){
	 echo "Image uploaded"."<br></br>";
	}else{
	 echo "Error uploading image";
	}
		 
     $sql_all="UPDATE  product,product_image set product.product_name='$product_name',product.product_price='$product_price',
	 color='$product_color',quantity='$product_quantity',
	 product.product_size='$product_size',product.category_name_id=$category_name_id,product.gender_type_id=$gender_type_id,
     product.shop_id=$shop_id,product_image.image_name='$full_image_path1'
	 WHERE product.product_id=$post_id AND product_image.product_id=$post_id";
    if(Shoppers::$DB->link->query($sql_all)){
		echo "succesffuly updated";
	}
	else{
		echo "failed to update";
	}

	 }

	 else if($image_name2!=null){
   $image_path2=$images_folder."/".$imageName2;
  $full_image_path2=$host_folder.$image_path2;
   $image_name22 = base64_decode($image_name2);
	$fp = fopen("../".$image_path2, 'w');
	fwrite($fp, $image_name22);
	if(fclose($fp)){
	 echo "Image uploaded"."<br></br>";
	}else{
	 echo "Error uploading image";
	}

       $sql_all="UPDATE  product,product_image set product.product_name='$product_name',product.product_price='$product_price',
	   color='$product_color',quantity='$product_quantity',
	 product.product_size='$product_size',product.category_name_id=$category_name_id,product.gender_type_id=$gender_type_id,
     product.shop_id=$shop_id,product_image.image_name2='$full_image_path2'
	 WHERE product.product_id=$post_id AND product_image.product_id=$post_id";
     if(Shoppers::$DB->link->query($sql_all)){
		echo "succesffuly updated";
	}
	else{
		echo "failed to update";
	}

	 }
	else if($image_name3!=null){
	$image_path1=$images_folder."/".$imageName3;
$full_image_path3=$host_folder.$image_path3;
    $image_name33 = base64_decode($image_name3);
	$fp = fopen("../".$image_path3, 'w');
	fwrite($fp, $image_name33);
	if(fclose($fp)){
	 echo "Image uploaded3"."<br></br>";
	}else{
	 echo "Error uploading image";
	}

       $sql_all="UPDATE  product,product_image set product.product_name='$product_name',product.product_price='$product_price',
	   color='$product_color',quantity='$product_quantity',
	 product.product_size='$product_size',product.category_name_id=$category_name_id,product.gender_type_id=$gender_type_id,
     product.shop_id=$shop_id,product_image.image_name3='$full_image_path3'
	 WHERE product.product_id=$post_id AND product_image.product_id=$post_id";
    if(Shoppers::$DB->link->query($sql_all)){
		echo "succesffuly updated";
	}
	else{
		echo "failed to update";
	}

	 }

	 else if($image_name1==null && $image_name2==null && $image_name3==null){
        $sql_all="UPDATE  product,product_image set product.product_name='$product_name',product.product_price='$product_price',
		color='$product_color',quantity='$product_quantity',
	 product.product_size='$product_size',product.category_name_id=$category_name_id,product.gender_type_id=$gender_type_id,
     product.shop_id=$shop_id
	 WHERE product.product_id=$post_id AND product_image.product_id=$post_id";
     if(Shoppers::$DB->link->query($sql_all)){
		echo "succesffuly updated5";
	}
	else{
		echo "failed to update";
	}
   
	 }
	
 }


 


	


 
