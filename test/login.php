<?php
class Shop_owners{
    public $owner_full_name;
    public $owner_last_name;
    public $owner_phone_number;
    public $owner_email;
    public $owner_password;
    public $owner_verification_key;
    public $shop_name;
    public $district_id;
    public $shop_contact;
    public $shop_description;
    public $shop_logo;
    public $shop_street_name;
    

    static function createOwner($owner_full_name,$owner_shop_contact,$owner_email,
    $owner_password,$owner_verification_key,$shop_details){
      $values=array("owner_full_name"=>$owner_full_name,
      "owner_shop_contact"=>$owner_shop_contact,"owner_email"=>$owner_email,
      "owner_password"=>$owner_password,"activation_key"=>$owner_verification_key);
      $table="shop_owner";
      $table2="shop";
      $table3="shop_view";
      Shoppers::$DB->start();
      $inserted_id=Shoppers::$DB->insert($table,$values);
      $values3=array("shop_id"=>$inserted_id);
      
      
      $values2=array("shop_name"=>$shop_details['shop_name'],"district_id"=>$shop_details['district_id'],
      "street_name"=>$shop_details['street_name'],"shop_logo"=>$shop_details['shop_logo'],
      "shop_description"=>$shop_details['shop_description'],
      "owner_id"=>$inserted_id);
      if(!Shoppers::$DB->insert($table2,$values2)){
          echo "failed";
          Shoppers::$DB->rollBack();
          return false;
      }
      
      else{
          if(Shoppers::$DB->insert($table3,$values3)){
                  Shoppers::$DB->commit();
                    echo "view added";
          return true;
            
          }
          else{
              echo "view failed";
          }
          
       
      }

      }   

      // to update the shop details
      static function updateShop($shop_id,$values,$where){
        $table="shop";
        $where="shop_id=$shop_id";
        if(Shoppers::$DB->update($table,$values,$where)){
          return true;
        }
        else 
        return false;
      }
      // update activation key
       static function updateKey($owner_id,$values,$where){
        $table="shop_owner";
        $where="owner_id=$owner_id";
        if(Shoppers::$DB->update($table,$values,$where)){
          return true;
        }
        else 
        return false;
      }
      // to update owner details
      static function updateOwner($owner_id,$values,$where){
      $table="shop_owner";
      $where="owner_id=$owner_id";
      if(Shoppers::$DB->update($table,$values,$where)){
        return true;
      }
      else{
        return false;
      }
      }
     
        //delete shops // owner
       static function deleteShop($shop_id){
        
        $sql="delete shop,shop_owner from shop inner join shop_owner on shop.owner_id=shop_owner.owner_id where shop_id=$shop_id";       
        
         if(Shoppers::$DB->link->query($sql)){
           echo "successfully deleted";
         }
         else{
           echo "could not delete shop";
         }

      }
      // to get all category gender type
      static function getGendersType(){
        $table="all_gender_type";
         if($genders_type=Shoppers::$DB->select($table,null,null)){
           echo json_encode(array("response"=>"ok","genders_type"=>$genders_type));
         }
         else{
           echo json_encode(array("response"=>"failed","message"=>"no genderes found"));
         }
           
      }
      // to delete gender type by admin
      static function deleteGenderType($gender_type_id){
      $table="all_gender_type";
      $where="gender_type_id=$gender_type_id";
      if(Shoppers::$DB->delete($table,$where)){
        echo "successfully deleted";
      }
      else{
        echo "failed to delete ";
      }
      }
       // to delete shop gender type 
       static function deleteShopGenderType($shop_gender_type_id,$shop_id){
         $table="shop_gender_type";
         $where="gender_type_id=$shop_gender_type_id and shop_id=$shop_id";
         if(Shoppers::$DB->delete($table,$where))
         echo "successfully deleted";
        else
         echo "failed to delete ";
       }
       static function deleteShopCategoryName($shop_id,$shop_gender_type_id,$category_name_id){
         $table="shop_category_name";
         $where="shop_id=$shop_id and gender_type_id=$shop_gender_type_id and category_name_id=$category_name_id";
        if(Shoppers::$DB->delete($table,$where))
         echo "successfully deleted";
        else
         echo "failed to delete shop category name";
         
       }
       static function loginOwner($owner_shop_contact,$owner_password,$owner_verification_key)
       {
         $table="shop_owner";
         $where="owner_shop_contact='{$owner_shop_contact}' and owner_password='{$owner_password}' and activation_key='{$owner_verification_key}'";
          if($result=Shoppers::$DB->select($table,null,$where)){
          //  echo "succesfully login"."<br><br/>";
            echo json_encode(array("response"=>"ok","message"=>$result));
          }
          else{
            echo "login failed please try again";
          }

       }

       // admin login 
 static function adminLoginOwner($username,$password)
       {
         $table="admin_login";
         $where="user_name='{$username}' and password='{$password}'";
          if($result=Shoppers::$DB->select($table,null,$where)){
          //  echo "succesfully login"."<br><br/>";
            echo json_encode(array("response"=>"ok","message"=>$result));
          }
          else{
            echo "login failed please try again";
          }

       }


       // generating random numbers for activation keys
       static function randomKeys($digit=4){
        $i=0;
        $spin=0;
        while($i<$digit){

        $pin=mt_rand(0000,9999);
        $i++;
        return $pin;
}
}


}

?>
